import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HelperValidator }  from './../validators/helpervalidator';
import { LoaderService } from '../services/loader/loader.service';
import { ActivatedRoute } from '@angular/router';
import { ConsumeHttpService} from './../services/api/consume-http.service';
import { StorageService } from './../services/storage/storage.service';
import { AuthenticationService } from './../services/login/authentication.service';
import { Config } from '../config/config';

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.page.html',
    styleUrls: ['./login-page.page.scss']
})
export class LoginPagePage implements OnInit
{
    showOTPInput: boolean = false;
    slideOneForm: FormGroup;
    submitAttempt: boolean = false;
    Data: any[] = [];
    hash: any;
    itemsData: any;
    textLogin:string = 'Iniciar sesión';

    constructor(
        public consumeApi:ConsumeHttpService,
        public forgotCtrl: AlertController,
        public formBuilder: FormBuilder,
        private ionLoader: LoaderService,
        private storage: StorageService,
        private authService: AuthenticationService,
        private activatedRouteute: ActivatedRoute
    )
    {
      this.slideOneForm = this.formBuilder.group({
        firstname: [''],
        lastname: [''],
        birthdate: [''],
        //email: ['',Validators.compose([Validators.required, HelperValidator.isEmail])],
        //password: ['',Validators.compose([Validators.required,Validators.minLength(6)])]
        email: [''],
        password: ['']
      });
    }

    ngOnInit()
    {
      this.activatedRouteute.queryParams.subscribe(params => {
        console.log(params);
        if(params.login !== undefined){
          let checkLogin = Boolean(JSON.parse(params.login));
          this.prelogin(checkLogin);
        }
      });
    }

    prelogin(status:boolean)
    {
        this.showOTPInput = status;
        this.slideOneForm.reset();
        if(this.showOTPInput){
            //this.slideOneForm.get('firstname').setValidators([Validators.required, Validators.minLength(4)]);
            //this.slideOneForm.get('firstname').updateValueAndValidity();
            //this.slideOneForm.get('lastname').setValidators([Validators.required, Validators.minLength(4)]);
            //this.slideOneForm.get('lastname').updateValueAndValidity();
            //this.slideOneForm.get('birthdate').setValidators([Validators.required]);
            //this.slideOneForm.get('birthdate').updateValueAndValidity();
            this.textLogin = 'Registrar';
        }
        else{
          //this.slideOneForm.get('firstname').clearValidators();
          //this.slideOneForm.get('firstname').updateValueAndValidity();
          //this.slideOneForm.get('lastname').clearValidators();
          //this.slideOneForm.get('lastname').updateValueAndValidity();
          //this.slideOneForm.get('birthdate').clearValidators();
          //this.slideOneForm.get('birthdate').updateValueAndValidity();
          this.textLogin = 'Iniciar sesión';
        }
    }

    async login()
    {
      this.submitAttempt = true;

        if(this.slideOneForm.valid)
        {
           this.ionLoader.present().then(()=>{
             this.authService.login(this.slideOneForm.value).subscribe(data => {
                  this.itemsData = data;
                  this.ionLoader.dismiss();
                  if(this.itemsData.response)
                  {
                      this.storage.setString(Config.TOKEN_KEY, this.itemsData.data.jwt);
                      this.authService.resetAuth();
                      this.ionLoader.showToast("Datos correctos",'bottom','/home',null);
                  }
                  else{
                      this.ionLoader.showToast(this.itemsData.message,'bottom',null,null);
                  }
              });
            });
        }
    }

    register()
    {
        this.submitAttempt = true;

        if(this.slideOneForm.valid)
        {
          this.ionLoader.present().then(()=>{
            this.authService.register(this.slideOneForm.value).subscribe(data => {
                 this.itemsData = data;
                 this.ionLoader.dismiss();
                 if(this.itemsData.response)
                 {
                    this.slideOneForm.reset();
                    this.ionLoader.showToast(this.itemsData.message,'bottom','/login-page',{ login: false });
                 }
                 else{
                     this.ionLoader.showToast(this.itemsData.message,'bottom',null,null);
                 }
             });
          });
        }
    }


}
