import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Config } from "./../../config/config";
import { StorageService } from './../storage/storage.service';
import { ConsumeHttpService} from './../api/consume-http.service';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

    isAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
    token = '';

    constructor(
        private storage: StorageService,
        private apiService: ConsumeHttpService
      ) {
        this.loadToken();
    }

    async loadToken() {
        //const token = await Storage.get({ key: Config.TOKEN_KEY });
        this.storage.getString(Config.TOKEN_KEY).then((token) => {
          if (token && token.value)
          {
            this.token = token.value;
            this.isAuthenticated.next(true);
          } else {
            this.isAuthenticated.next(false);
          }
        });
    }

    resetAuth(){
      this.isAuthenticated.next(true);
    }

    login(credentials: {email, password}): Observable<any>
    {
        let httpParams = new HttpParams()
        .append("email", credentials['email'])
        .append("password", credentials['password']);
        const URL = Config.URL+"login";
        return this.apiService.pushRequest(URL,httpParams);
    }

    register(user: {firstname, lastname, birthdate, email, password}): Observable<any>
    {
        let httpParams = new HttpParams()
        .append("firstname", user['firstname'])
        .append("lastname", user['lastname'])
        .append("birthdate", user['birthdate'])
        .append("email", user['email'])
        .append("password", user['password']);
        const URL = Config.URL+"registro";
        return this.apiService.pushRequest(URL,httpParams);
    }

    logout(): Promise<void> {
        console.log("logout");
        this.isAuthenticated.next(false);
        return this.storage.clear();
    }
}
