import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse,HttpHeaders,HttpParams } from '@angular/common/http';
import { Config } from "./../../config/config";
import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { map, tap, switchMap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ConsumeHttpService {

  conexion:any;
  postData = [];

  constructor(
    public http: HttpClient)
    {

    }

    handleError(error: HttpErrorResponse) {
      if (error.error instanceof ErrorEvent) {

        console.error('An error occurred:', error.error.message);
      } else {

        console.error(
          `Backend returned code ${error.status}, ` +
          `body was: ${error.error}`);
      }

      return throwError(
        'Something bad happened; please try again later.');
    }

    pushRequest(url,httpParams): Observable<any>
    {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append("Content-Type", "application/x-www-form-urlencoded");

      return this.http.post<any>(url,null,{headers: headers,params: httpParams}).pipe(
        retry(2),
        catchError(this.handleError),
        tap(_ => {
            return true;
        })
      )
    }

    getRequest(url,authToken): Observable<any>
    {
        const header = new HttpHeaders().set('Authorization',  `Bearer ${authToken}`);
        const headers = { headers: header };

        const URL = Config.URL+url;
        return this.http.get(URL,headers).pipe(
          retry(2),
          catchError(this.handleError),
          tap(_ => {
              return true;
          })
        )
    }

}
