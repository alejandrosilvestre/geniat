import { TestBed } from '@angular/core/testing';

import { ConsumeHttpService } from './consume-http.service';

describe('ConsumeHttpService', () => {
  let service: ConsumeHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConsumeHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
