import { Injectable } from '@angular/core';
import { LoadingController,ToastController } from '@ionic/angular';
import { Router } from '@angular/router';


@Injectable({
    providedIn: 'root'
})
export class LoaderService {

    isLoading = false;
    isToast = false;
    loader: any;
    toast:any;
    callback:any;

    constructor(
        public loadingController: LoadingController,
        public toastCtrl: ToastController,
        private router: Router
    ) { }

    async present() {
        if (!this.isLoading) {
            this.isLoading = true
            this.loader = await this.loadingController.create({
              message: 'Cargando...',
              //duration: 4000
            });
            return await this.loader.present();
        }
    }

    async dismiss()
    {
        if (this.loader) {
            this.loader.dismiss()
            this.loader = null
            this.isLoading = false
        }
    }

    async showToast(data: any,position: any,url:any,params:any) {

      this.toast = await this.toastCtrl.create({
        message: data,
        duration: 1000,
        position: position,
        cssClass: 'dark-trans'
      });
      let urlR = url;
      let paramsR = params;
      this.toast.onDidDismiss().then((val) => {
        this.redirectUrl(urlR,paramsR);
      });
      return await this.toast.present();

    }

    redirectUrl(url:string,params:any){
      if(url != null){
        let router = this.router;
        setTimeout(function(){
          console.log(url);
          router.navigate([url], { queryParams: params });
        }, 300);
      }
    }
}
