
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Plugins } from '@capacitor/core';
import { StorageService } from './../storage/storage.service';
import { Config } from  './../../config/config';


@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  token:any;

  constructor(
    private router: Router,
    private storage: StorageService
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    this.storage.getString(Config.TOKEN_KEY).then((token) => {
      this.token = token;
    });

    let request = req;

    if (this.token && this.token.value) {
      request = req.clone({
        setHeaders: {
          authorization: `Bearer ${ this.token.value }`
        }
      });
    }

    return next.handle(request).pipe(
      catchError((err: HttpErrorResponse) => {

        if (err.status === 401) {
          this.router.navigateByUrl('/login-page');
        }

        return throwError( err );

      })
    );
  }

}
