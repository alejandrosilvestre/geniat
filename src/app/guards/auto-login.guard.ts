import { Injectable } from '@angular/core';
import { CanLoad, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from './../services/login/authentication.service';
import { filter, map, take } from 'rxjs/operators';
import { StorageService } from './../services/storage/storage.service';
import { Config } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class AutoLoginGuard implements CanLoad {

    constructor(
        private authService: AuthenticationService,
        private router: Router,
        private storage: StorageService
    ) { }

    canLoad(): Observable<boolean> {
        return this.authService.isAuthenticated.pipe(
            filter(val => val !== null),
            take(1),
            map(isAuthenticated => {
                console.log('Found previous token, automatic login');
                if (isAuthenticated) {
                  this.storage.getString(Config.TOKEN_KEY).then((token:any) => {
                    if(token)
                    {
                      this.router.navigateByUrl('/home', { replaceUrl:true });
                    }
                  });

                } else {
                  return true;
                }
            })
        );
    }
}
