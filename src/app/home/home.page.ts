import { Component,OnInit, ViewChild } from '@angular/core';
import { StorageService } from './../services/storage/storage.service';
import { ConsumeHttpService} from './../services/api/consume-http.service';
import { Config } from './../config/config';
import { Observable } from 'rxjs';
import { IonInfiniteScroll, IonVirtualScroll } from '@ionic/angular';
import { AuthenticationService } from './../services/login/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  public data:any;
  public _isSkeleton:boolean=true;
  itemsData: any;
  total:number = 0;
  totalRows:number = 0;
  checktotal = false;
  filterTerm: string;

  dataList = [];

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonVirtualScroll) virtualScroll: IonVirtualScroll;

  constructor(
    private storage: StorageService,
    public consumeApi:ConsumeHttpService,
    private authService: AuthenticationService,
    private router: Router
  ) {}

  ngOnInit(){
    this.getList(10);
  }

  getList(total:number) {
    let m = false;
    this.total = this.total + total;
    if(this.checktotal)
    {
      return false;
    }
    this.storage.getString(Config.TOKEN_KEY).then((token) => {

      this.register('lista',token.value).subscribe(data => {
        this.itemsData = data;

        if(this.itemsData.response)
        {
          console.log(this.itemsData.data);
          this.data = this.itemsData.data.resultados;
          if(this.total>this.data.length && !this.checktotal){
            this.totalRows = this.total - this.data.length;
            this.total = this.total - this.totalRows;
          }


          for (let i = 0; i < this.total; i++)
          {
            var matchedIndex = this.dataList.map(function (obj) { return obj.idModelo; }).indexOf(this.data[i].idModelo);

            if(matchedIndex == -1)
            {
              this.dataList.push({
                idModelo: this.data[i].idModelo,
                nombreMarca: this.data[i].nombreMarca,
                nombreModelo: this.data[i].nombreModelo
              });
            }
          }

          if(this.total == this.data.length){
            this.checktotal = true;
          }

          //console.log(this.dataList);
        }

      });

    });
  }

  loadData(event) {

    setTimeout(() => {

      this.getList(10)

      event.target.complete();

      //this.virtualScroll.checkEnd();

      if (this.dataList.length == 1000) {
        event.target.disabled = true;
      }
    }, 100);
  }


  register(url,authToken): Observable<any>
  {
    return this.consumeApi.getRequest(url,authToken);
  }

  async logout() {
    await this.authService.logout();
    this.router.navigateByUrl('/login-page', { replaceUrl: true });
  }

}
