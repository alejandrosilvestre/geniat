import { FormControl,AbstractControl, ValidationErrors } from '@angular/forms';

export class HelperValidator {

    static isEmail(control: FormControl){

    	let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let result = re.test(control.value);

        if (!result) {
        	return {
        		'fail' : true
        	}
        }

        return null;
    }
}
