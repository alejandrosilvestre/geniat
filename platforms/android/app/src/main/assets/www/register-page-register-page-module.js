(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["register-page-register-page-module"],{

/***/ "Gpnm":
/*!*******************************************************!*\
  !*** ./src/app/register-page/register-page.module.ts ***!
  \*******************************************************/
/*! exports provided: RegisterPagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPagePageModule", function() { return RegisterPagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _register_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./register-page-routing.module */ "vt+q");
/* harmony import */ var _register_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./register-page.page */ "XZwm");







let RegisterPagePageModule = class RegisterPagePageModule {
};
RegisterPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _register_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["RegisterPagePageRoutingModule"]
        ],
        declarations: [_register_page_page__WEBPACK_IMPORTED_MODULE_6__["RegisterPagePage"]]
    })
], RegisterPagePageModule);



/***/ }),

/***/ "XZwm":
/*!*****************************************************!*\
  !*** ./src/app/register-page/register-page.page.ts ***!
  \*****************************************************/
/*! exports provided: RegisterPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPagePage", function() { return RegisterPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_register_page_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./register-page.page.html */ "m+kj");
/* harmony import */ var _register_page_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./register-page.page.scss */ "zKCE");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let RegisterPagePage = class RegisterPagePage {
    constructor() { }
    ngOnInit() {
    }
};
RegisterPagePage.ctorParameters = () => [];
RegisterPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-register-page',
        template: _raw_loader_register_page_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_register_page_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], RegisterPagePage);



/***/ }),

/***/ "m+kj":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/register-page/register-page.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>register-page</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n");

/***/ }),

/***/ "vt+q":
/*!***************************************************************!*\
  !*** ./src/app/register-page/register-page-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: RegisterPagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPagePageRoutingModule", function() { return RegisterPagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _register_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./register-page.page */ "XZwm");




const routes = [
    {
        path: '',
        component: _register_page_page__WEBPACK_IMPORTED_MODULE_3__["RegisterPagePage"]
    }
];
let RegisterPagePageRoutingModule = class RegisterPagePageRoutingModule {
};
RegisterPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], RegisterPagePageRoutingModule);



/***/ }),

/***/ "zKCE":
/*!*******************************************************!*\
  !*** ./src/app/register-page/register-page.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZWdpc3Rlci1wYWdlLnBhZ2Uuc2NzcyJ9 */");

/***/ })

}]);
//# sourceMappingURL=register-page-register-page-module.js.map