(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-page-login-page-module"],{

/***/ "LKdp":
/*!***********************************************!*\
  !*** ./src/app/login-page/login-page.page.ts ***!
  \***********************************************/
/*! exports provided: LoginPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPagePage", function() { return LoginPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_login_page_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./login-page.page.html */ "R2JP");
/* harmony import */ var _login_page_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login-page.page.scss */ "NjJD");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _services_loader_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/loader/loader.service */ "YLvv");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _services_api_consume_http_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./../services/api/consume-http.service */ "uetO");
/* harmony import */ var _services_storage_storage_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./../services/storage/storage.service */ "E2f4");
/* harmony import */ var _services_login_authentication_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./../services/login/authentication.service */ "BsE8");
/* harmony import */ var _config_config__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../config/config */ "0np6");












let LoginPagePage = class LoginPagePage {
    constructor(consumeApi, forgotCtrl, formBuilder, ionLoader, storage, authService, activatedRouteute) {
        this.consumeApi = consumeApi;
        this.forgotCtrl = forgotCtrl;
        this.formBuilder = formBuilder;
        this.ionLoader = ionLoader;
        this.storage = storage;
        this.authService = authService;
        this.activatedRouteute = activatedRouteute;
        this.showOTPInput = false;
        this.submitAttempt = false;
        this.Data = [];
        this.textLogin = 'Iniciar sesión';
        this.slideOneForm = this.formBuilder.group({
            firstname: [''],
            lastname: [''],
            birthdate: [''],
            //email: ['',Validators.compose([Validators.required, HelperValidator.isEmail])],
            //password: ['',Validators.compose([Validators.required,Validators.minLength(6)])]
            email: [''],
            password: ['']
        });
    }
    ngOnInit() {
        this.activatedRouteute.queryParams.subscribe(params => {
            console.log(params);
            if (params.login !== undefined) {
                let checkLogin = Boolean(JSON.parse(params.login));
                this.prelogin(checkLogin);
            }
        });
    }
    prelogin(status) {
        this.showOTPInput = status;
        this.slideOneForm.reset();
        if (this.showOTPInput) {
            //this.slideOneForm.get('firstname').setValidators([Validators.required, Validators.minLength(4)]);
            //this.slideOneForm.get('firstname').updateValueAndValidity();
            //this.slideOneForm.get('lastname').setValidators([Validators.required, Validators.minLength(4)]);
            //this.slideOneForm.get('lastname').updateValueAndValidity();
            //this.slideOneForm.get('birthdate').setValidators([Validators.required]);
            //this.slideOneForm.get('birthdate').updateValueAndValidity();
            this.textLogin = 'Registrar';
        }
        else {
            //this.slideOneForm.get('firstname').clearValidators();
            //this.slideOneForm.get('firstname').updateValueAndValidity();
            //this.slideOneForm.get('lastname').clearValidators();
            //this.slideOneForm.get('lastname').updateValueAndValidity();
            //this.slideOneForm.get('birthdate').clearValidators();
            //this.slideOneForm.get('birthdate').updateValueAndValidity();
            this.textLogin = 'Iniciar sesión';
        }
    }
    login() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.submitAttempt = true;
            if (this.slideOneForm.valid) {
                this.ionLoader.present().then(() => {
                    this.authService.login(this.slideOneForm.value).subscribe(data => {
                        this.itemsData = data;
                        this.ionLoader.dismiss();
                        if (this.itemsData.response) {
                            this.storage.setString(_config_config__WEBPACK_IMPORTED_MODULE_11__["Config"].TOKEN_KEY, this.itemsData.data.jwt);
                            this.authService.resetAuth();
                            this.ionLoader.showToast("Datos correctos", 'bottom', '/home', null);
                        }
                        else {
                            this.ionLoader.showToast(this.itemsData.message, 'bottom', null, null);
                        }
                    });
                });
            }
        });
    }
    register() {
        this.submitAttempt = true;
        if (this.slideOneForm.valid) {
            this.ionLoader.present().then(() => {
                this.authService.register(this.slideOneForm.value).subscribe(data => {
                    this.itemsData = data;
                    this.ionLoader.dismiss();
                    if (this.itemsData.response) {
                        this.slideOneForm.reset();
                        this.ionLoader.showToast(this.itemsData.message, 'bottom', '/login-page', { login: false });
                    }
                    else {
                        this.ionLoader.showToast(this.itemsData.message, 'bottom', null, null);
                    }
                });
            });
        }
    }
};
LoginPagePage.ctorParameters = () => [
    { type: _services_api_consume_http_service__WEBPACK_IMPORTED_MODULE_8__["ConsumeHttpService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"] },
    { type: _services_loader_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"] },
    { type: _services_storage_storage_service__WEBPACK_IMPORTED_MODULE_9__["StorageService"] },
    { type: _services_login_authentication_service__WEBPACK_IMPORTED_MODULE_10__["AuthenticationService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"] }
];
LoginPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-login-page',
        template: _raw_loader_login_page_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_login_page_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], LoginPagePage);



/***/ }),

/***/ "NjJD":
/*!*************************************************!*\
  !*** ./src/app/login-page/login-page.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".main {\n  height: 100vh;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n}\n\n@media screen and (min-width: 767px) {\n  ion-card {\n    width: 600px;\n    margin: auto;\n  }\n}\n\nion-card {\n  --background: #fff;\n  box-shadow: none;\n  -webkit-box-shadow: none;\n  overflow: scroll;\n}\n\n.input {\n  background-color: #f0f0f0;\n  border: 1px solid #d2d2d2;\n  border-radius: 9px;\n  font-size: 0.9em !important;\n  padding-left: 10px !important;\n}\n\n.otpinput {\n  letter-spacing: 30px;\n  -webkit-padding-end: 0;\n  --padding-end: 0;\n  font-size: 30px;\n  border: none;\n  background: white;\n}\n\n.white {\n  color: white;\n}\n\n.small {\n  font-size: 13px;\n}\n\n.small a {\n  text-decoration: unset !important;\n}\n\n.button-color {\n  background-color: var(--ion-color-mytheme);\n}\n\n.logo {\n  width: 1.25em !important;\n}\n\n.grid {\n  height: 100vh;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n}\n\n.row {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n}\n\n.img-logo {\n  height: 120px;\n  width: 120px;\n}\n\n.fire-logo {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  padding-bottom: 30px;\n}\n\n.bold {\n  font-weight: bold;\n}\n\n.block {\n  display: block;\n}\n\n.transition {\n  background: linear-gradient(to right, #f57c00 14%, #ffca00 96%);\n  border-radius: 5px;\n}\n\n.btn-color {\n  color: #ffa000;\n}\n\n.error {\n  color: red;\n  text-align: center;\n  display: block;\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL2xvZ2luLXBhZ2UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0MsYUFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0FBQUQ7O0FBR0E7RUFDQztJQUNFLFlBQUE7SUFDQSxZQUFBO0VBQUQ7QUFDRjs7QUFFQTtFQUNDLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSx3QkFBQTtFQUNBLGdCQUFBO0FBQUQ7O0FBRUE7RUFDQyx5QkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSwyQkFBQTtFQUNDLDZCQUFBO0FBQ0Y7O0FBQ0E7RUFDQyxvQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0FBRUQ7O0FBQ0E7RUFDQyxZQUFBO0FBRUQ7O0FBQ0E7RUFDQyxlQUFBO0FBRUQ7O0FBREM7RUFDQyxpQ0FBQTtBQUdGOztBQUFBO0VBQ0MsMENBQUE7QUFHRDs7QUFEQTtFQUNDLHdCQUFBO0FBSUQ7O0FBRkE7RUFDQyxhQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7QUFLRDs7QUFIQTtFQUNDLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FBTUQ7O0FBSkE7RUFDQyxhQUFBO0VBQ0EsWUFBQTtBQU9EOztBQUxBO0VBQ0MsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0FBUUQ7O0FBTkE7RUFDQyxpQkFBQTtBQVNEOztBQVBBO0VBQ0MsY0FBQTtBQVVEOztBQVJBO0VBQ0MsK0RBQUE7RUFDQyxrQkFBQTtBQVdGOztBQVRBO0VBQ0MsY0FBQTtBQVlEOztBQVZBO0VBQ0MsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0FBYUQiLCJmaWxlIjoibG9naW4tcGFnZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBzdHlsZSBmb3IgYXV0aCBwYWdlc1xuLm1haW4ge1xuXHRoZWlnaHQ6IDEwMHZoO1xuXHRkaXNwbGF5OiBmbGV4O1xuXHRmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuXHRqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDogNzY3cHgpIHtcblx0aW9uLWNhcmQge1xuXHQgIHdpZHRoOiA2MDBweDtcblx0ICBtYXJnaW46IGF1dG87XG5cdH1cbn1cbmlvbi1jYXJkIHtcblx0LS1iYWNrZ3JvdW5kOiAjZmZmO1xuXHRib3gtc2hhZG93OiBub25lO1xuXHQtd2Via2l0LWJveC1zaGFkb3c6IG5vbmU7XG5cdG92ZXJmbG93OiBzY3JvbGw7XG59XG4uaW5wdXQge1xuXHRiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjQwLCAyNDAsIDI0MCk7XG5cdGJvcmRlcjogMXB4IHNvbGlkIHJnYigyMTAsIDIxMCwgMjEwKTtcblx0Ym9yZGVyLXJhZGl1czogOXB4O1xuXHRmb250LXNpemU6IDAuOWVtICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctbGVmdDogMTBweCAhaW1wb3J0YW50O1xufVxuLm90cGlucHV0IHtcblx0bGV0dGVyLXNwYWNpbmc6IDMwcHg7XG5cdC13ZWJraXQtcGFkZGluZy1lbmQ6IDA7XG5cdC0tcGFkZGluZy1lbmQ6IDA7XG5cdGZvbnQtc2l6ZTogMzBweDtcblx0Ym9yZGVyOiBub25lO1xuXHRiYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cblxuLndoaXRlIHtcblx0Y29sb3I6IHdoaXRlO1xufVxuXG4uc21hbGwge1xuXHRmb250LXNpemU6IDEzcHg7XG5cdGEge1xuXHRcdHRleHQtZGVjb3JhdGlvbjogdW5zZXQgIWltcG9ydGFudDtcblx0fVxufVxuLmJ1dHRvbi1jb2xvciB7XG5cdGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1teXRoZW1lKTtcbn1cbi5sb2dvIHtcblx0d2lkdGg6IDEuMjVlbSAhaW1wb3J0YW50O1xufVxuLmdyaWQge1xuXHRoZWlnaHQ6IDEwMHZoO1xuXHRkaXNwbGF5OiBmbGV4O1xuXHRmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuXHRqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbi5yb3cge1xuXHRkaXNwbGF5OiBmbGV4O1xuXHRmbGV4LWRpcmVjdGlvbjogcm93O1xuXHRqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbi5pbWctbG9nbyB7XG5cdGhlaWdodDogMTIwcHg7XG5cdHdpZHRoOiAxMjBweDtcbn1cbi5maXJlLWxvZ28ge1xuXHRkaXNwbGF5OiBmbGV4O1xuXHRmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuXHRqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcblx0YWxpZ24taXRlbXM6IGNlbnRlcjtcblx0cGFkZGluZy1ib3R0b206IDMwcHg7XG59XG4uYm9sZCB7XG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmJsb2NrIHtcblx0ZGlzcGxheTogYmxvY2s7XG59XG4udHJhbnNpdGlvbiB7XG5cdGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgI2Y1N2MwMCAxNCUsICNmZmNhMDAgOTYlKTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuLmJ0bi1jb2xvciB7XG5cdGNvbG9yOiAjZmZhMDAwO1xufVxuLmVycm9yIHtcblx0Y29sb3I6IHJlZDtcblx0dGV4dC1hbGlnbjogY2VudGVyO1xuXHRkaXNwbGF5OiBibG9jaztcblx0Zm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4iXX0= */");

/***/ }),

/***/ "R2JP":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login-page/login-page.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content no-padding>\n\t<div class=\"main\">\n\t\t<ion-card no-margin>\n\t\t\t<form [formGroup]=\"slideOneForm\">\n\t\t\t\t<ion-card-content>\n                    <ion-row class=\"row\">\n                        <div class=\"fire-logo\">\n                        <ion-text padding>\n                            <h2 no-margin margin-vertical text-center>{{textLogin}}</h2>\n                        </ion-text>\n                        </div>\n                    </ion-row>\n                    <ion-row *ngIf=\"showOTPInput\">\n                      <ion-col>\n                          <ion-input clearInput placeholder=\"Nombre\" class=\"input\" padding-horizontal\n                          clear-input=\"true\" formControlName=\"firstname\"></ion-input>\n                          <div *ngIf=\"(slideOneForm.controls.firstname.dirty || slideOneForm.controls.firstname.touched) && slideOneForm.controls.firstname.errors\" class=\"errors\">\n                              <ion-label text-wrap color=\"danger\" *ngIf=\"slideOneForm.controls.firstname.errors?.required\">Nombre requerido...</ion-label>\n                              <ion-label text-wrap color=\"danger\" *ngIf=\"slideOneForm.controls.firstname.errors?.minlength\">El nombre debe tener 6 caracteres...</ion-label>\n                          </div>\n                      </ion-col>\n                    </ion-row>\n                    <ion-row *ngIf=\"showOTPInput\">\n                      <ion-col>\n                          <ion-input clearInput placeholder=\"Apellidos\" class=\"input\" padding-horizontal\n                          clear-input=\"true\" formControlName=\"lastname\"></ion-input>\n                          <div *ngIf=\"(slideOneForm.controls.lastname.dirty || slideOneForm.controls.lastname.touched) && slideOneForm.controls.lastname.errors\" class=\"errors\">\n                              <ion-label text-wrap color=\"danger\" *ngIf=\"slideOneForm.controls.lastname.errors?.required\">Apellidos requeridos...</ion-label>\n                              <ion-label text-wrap color=\"danger\" *ngIf=\"slideOneForm.controls.lastname.errors?.minlength\">Los apellidos deben tener 6 caracteres...</ion-label>\n                          </div>\n                      </ion-col>\n                    </ion-row>\n                    <ion-row *ngIf=\"showOTPInput\">\n                      <ion-col>\n                          <!--<ion-input clearInput type=\"date\" placeholder=\"Fecha de nacimiento\" class=\"input\" padding-horizontal\n                          clear-input=\"true\" formControlName=\"birthdate\"></ion-input>-->\n                          <ion-datetime placeholder=\"Fecha de nacimiento\"  displayFormat=\"YYYY-MM-DD\" value=\"\" class=\"input\" padding-horizontal\n                            clear-input=\"true\" formControlName=\"birthdate\"></ion-datetime>\n                          <div *ngIf=\"(slideOneForm.controls.birthdate.dirty || slideOneForm.controls.birthdate.touched) && slideOneForm.controls.birthdate.errors\" class=\"errors\">\n                              <ion-label text-wrap color=\"danger\" *ngIf=\"slideOneForm.controls.birthdate.errors?.required\">Fecha de nacimiento requerida...</ion-label>\n                          </div>\n                      </ion-col>\n                    </ion-row>\n                    <ion-row>\n                        <ion-col>\n                            <ion-input clearInput type=\"email\" placeholder=\"Correo\" class=\"input\" padding-horizontal\n                            clear-input=\"true\" formControlName=\"email\"></ion-input>\n                            <div *ngIf=\"(slideOneForm.controls.email.dirty || slideOneForm.controls.email.touched) && slideOneForm.controls.email.errors\" class=\"errors\">\n                                <ion-label text-wrap color=\"danger\" *ngIf=\"slideOneForm.controls.email.errors?.required\">Correo requerido...</ion-label>\n                                <ion-label text-wrap color=\"danger\" *ngIf=\"!slideOneForm.controls.email.errors?.required && slideOneForm.controls.email.errors?.fail\">Correo invalido..</ion-label>\n                            </div>\n                        </ion-col>\n                    </ion-row>\n                    <ion-row>\n                        <ion-col>\n                            <ion-input clearInput type=\"password\" placeholder=\"Contraseña\" class=\"input\" padding-horizontal\n                            clear-input=\"true\" formControlName=\"password\"></ion-input>\n                            <div *ngIf=\"(slideOneForm.controls.password.dirty || slideOneForm.controls.password.touched) && slideOneForm.controls.password.errors\" class=\"errors\">\n                                <ion-label text-wrap color=\"danger\" *ngIf=\"slideOneForm.controls.password.errors?.required\">Contraseña requerida...</ion-label>\n                                <ion-label text-wrap color=\"danger\" *ngIf=\"slideOneForm.controls.password.errors?.minlength\">La contraseña debe tener 6 caracteres...</ion-label>\n                            </div>\n                        </ion-col>\n                    </ion-row>\n                    <ion-row>\n                        <ion-col>\n                            <ion-button *ngIf=\"showOTPInput\" expand=\"block\" (click)=\"register()\" color=\"undefined\" class=\"transition\">\n                                <strong class=\"white\">Registrar</strong>\n                            </ion-button>\n                            <ion-button *ngIf=\"!showOTPInput\" expand=\"block\" (click)=\"login()\" color=\"undefined\" class=\"transition\">\n                                <strong class=\"white\">Ingresar</strong>\n                            </ion-button>\n                        </ion-col>\n                    </ion-row>\n                    <ion-row *ngIf=\"!showOTPInput\">\n                        <ion-col>\n                            <ion-text text-center class=\"block bold\" (click)=\"prelogin(true)\">Registrarse</ion-text>\n                        </ion-col>\n                    </ion-row>\n                    <ion-row *ngIf=\"showOTPInput\">\n                      <ion-col>\n                          <ion-text text-center class=\"block bold\" (click)=\"prelogin(false)\">Iniciar sesión</ion-text>\n                      </ion-col>\n                  </ion-row>\n\t\t\t\t</ion-card-content>\n\t\t\t</form>\n\t\t</ion-card>\n\t</div>\n</ion-content>\n");

/***/ }),

/***/ "UFfV":
/*!*********************************************************!*\
  !*** ./src/app/login-page/login-page-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: LoginPagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPagePageRoutingModule", function() { return LoginPagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _login_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login-page.page */ "LKdp");




const routes = [
    {
        path: '',
        component: _login_page_page__WEBPACK_IMPORTED_MODULE_3__["LoginPagePage"]
    }
];
let LoginPagePageRoutingModule = class LoginPagePageRoutingModule {
};
LoginPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LoginPagePageRoutingModule);



/***/ }),

/***/ "YLvv":
/*!***************************************************!*\
  !*** ./src/app/services/loader/loader.service.ts ***!
  \***************************************************/
/*! exports provided: LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");




let LoaderService = class LoaderService {
    constructor(loadingController, toastCtrl, router) {
        this.loadingController = loadingController;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.isLoading = false;
        this.isToast = false;
    }
    present() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (!this.isLoading) {
                this.isLoading = true;
                this.loader = yield this.loadingController.create({
                    message: 'Cargando...',
                });
                return yield this.loader.present();
            }
        });
    }
    dismiss() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.loader) {
                this.loader.dismiss();
                this.loader = null;
                this.isLoading = false;
            }
        });
    }
    showToast(data, position, url, params) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.toast = yield this.toastCtrl.create({
                message: data,
                duration: 1000,
                position: position,
                cssClass: 'dark-trans'
            });
            let urlR = url;
            let paramsR = params;
            this.toast.onDidDismiss().then((val) => {
                this.redirectUrl(urlR, paramsR);
            });
            return yield this.toast.present();
        });
    }
    redirectUrl(url, params) {
        if (url != null) {
            let router = this.router;
            setTimeout(function () {
                console.log(url);
                router.navigate([url], { queryParams: params });
            }, 300);
        }
    }
};
LoaderService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
LoaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], LoaderService);



/***/ }),

/***/ "x2uj":
/*!*************************************************!*\
  !*** ./src/app/login-page/login-page.module.ts ***!
  \*************************************************/
/*! exports provided: LoginPagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPagePageModule", function() { return LoginPagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _login_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login-page-routing.module */ "UFfV");
/* harmony import */ var _login_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login-page.page */ "LKdp");







let LoginPagePageModule = class LoginPagePageModule {
};
LoginPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _login_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPagePageRoutingModule"]
        ],
        declarations: [_login_page_page__WEBPACK_IMPORTED_MODULE_6__["LoginPagePage"]]
    })
], LoginPagePageModule);



/***/ })

}]);
//# sourceMappingURL=login-page-login-page-module.js.map